import { CreditForm } from './components/Form/CreditForm';

function App() {
  return (
    <div className="bg-[#003456]">
      <div className="flex justify-center min-h-screen p-9 max-w-[850px] m-auto">
        <CreditForm />
      </div>
    </div>
  );
}

export default App;
