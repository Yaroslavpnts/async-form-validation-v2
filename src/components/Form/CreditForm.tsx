import { useEffect, useState } from 'react';
import { Formik, FormikProps, Form } from 'formik';
import cn from 'classnames';
import DatePicker from 'react-datepicker';
import { AsyncValidationFields, IInitialValues, SyncValidationFields } from './types/form.types';
import { InputField } from './components/InputField/InputField';
import { MySelect } from './components/Select/Select';
import { SuccessfulSubmit } from './components/SuccessfulSubmit/SuccessfulSubmit';
import { getValidationSchema } from './validation/validationSchema';
import { creditFormApi, initialSelectorsData } from '../../api/submitForm';
import 'react-datepicker/dist/react-datepicker.css';
import './index.css';

const initialFormData: IInitialValues = {
  [AsyncValidationFields.Email]: '',
  [SyncValidationFields.ActivityType]: '',
  [SyncValidationFields.DateCompleted]: null,
  [SyncValidationFields.CreditType]: '',
  [SyncValidationFields.CreditAmount]: '',
  [SyncValidationFields.Description]: '',
  [SyncValidationFields.Reflection]: '',
};

const maxDate = new Date();
maxDate.setDate(maxDate.getDate() - 1);

const formColumnStyles = 'w-full md:w-1/2 py-10 md:py-20 px-8 sm:px-20 md:px-14';

export const CreditForm: React.FC = () => {
  const [selectData, setSelectData] = useState(initialSelectorsData);
  const [successfulSubmit, setSuccessfulSubmit] = useState(false);
  const [submitValidation, setSubmitValidation] = useState(false);

  useEffect(() => {
    const getCreditFormData = async () => {
      const data = await creditFormApi.getDateForCreditForm();
      setSelectData(data);
    };
    getCreditFormData();
  }, []);

  return (
    <div className="flex items-center">
      {!successfulSubmit ? (
        <Formik
          initialValues={initialFormData}
          validationSchema={getValidationSchema(submitValidation)}
          validateOnBlur={false}
          validateOnChange={false}
          validateOnMount={true}
          onSubmit={async values => {
            try {
              await creditFormApi.submitForm(values);
              setSuccessfulSubmit(true);
            } catch (error) {
              alert('Something went wrong');
            }
          }}
        >
          {({
            values,
            setFieldValue,
            isSubmitting,
            isValidating,
            setFieldTouched,
            validateField,
            errors,
            touched,
          }: FormikProps<IInitialValues>) => (
            <Form className="flex flex-col md:flex-row w-full rounded-lg overflow-hidden">
              {Boolean(console.log({ isSubmitting }))}
              <div className={cn(formColumnStyles, 'md:py-32 bg-[#fff]')}>
                <h2 className="text-2xl md:text-3xl lg:text-4xl font-extrabold text-center mb-3">
                  Submit External Credit
                </h2>
                <p className="text-center mb-3 font-medium">Enter your credit details below</p>
                <InputField
                  name={AsyncValidationFields.Email}
                  placeholder={AsyncValidationFields.Email}
                  checkIcon={true}
                  isValidating={isValidating}
                  asyncValidating={true}
                  setFieldTouched={setFieldTouched}
                  validateField={validateField}
                  isSubmitting={isSubmitting}
                  setSubmitValidation={setSubmitValidation}
                />
                <MySelect
                  options={selectData.activityType.map(option => ({
                    value: option,
                    label: option,
                  }))}
                  name={SyncValidationFields.ActivityType}
                  placeholder="Select an Activity Type"
                  setFieldValue={setFieldValue}
                  validateField={validateField}
                  value={values.activityType}
                  touched={touched}
                  errors={errors}
                />
                <DatePicker
                  selected={values.dateCompleted}
                  onChange={(date: Date | null) => {
                    setFieldValue('dateCompleted', date, false).then(() => {
                      validateField('dateCompleted');
                    });
                  }}
                  maxDate={maxDate}
                  className="w-full py-3 px-4 bg-[#eeeeee] placeholder:text-slate-400 placeholder:font-medium"
                  placeholderText="date Completed"
                />
                {touched['dateCompleted'] && errors['dateCompleted'] && (
                  <div className="text-red-400">{errors['dateCompleted']}</div>
                )}
              </div>
              <div className={cn(formColumnStyles, 'bg-[#66869b]')}>
                <div className="mb-3">
                  <MySelect
                    options={selectData.creditType.map(option => ({
                      value: option,
                      label: option,
                    }))}
                    name={SyncValidationFields.CreditType}
                    placeholder="select a Credit Type"
                    setFieldValue={setFieldValue}
                    validateField={validateField}
                    value={values.creditType}
                    touched={touched}
                    errors={errors}
                  />
                  <MySelect
                    options={selectData.creditAmount.map(option => ({
                      value: option,
                      label: option,
                    }))}
                    name={SyncValidationFields.CreditAmount}
                    placeholder="select Credit Amount"
                    setFieldValue={setFieldValue}
                    validateField={validateField}
                    value={values.creditAmount}
                    touched={touched}
                    errors={errors}
                  />
                  <InputField
                    name={SyncValidationFields.Description}
                    placeholder={SyncValidationFields.Description}
                    setFieldTouched={setFieldTouched}
                    validateField={validateField}
                    isSubmitting={isSubmitting}
                    setSubmitValidation={setSubmitValidation}
                  />
                  <InputField
                    name={SyncValidationFields.Reflection}
                    placeholder={SyncValidationFields.Reflection}
                    setFieldTouched={setFieldTouched}
                    validateField={validateField}
                    isSubmitting={isSubmitting}
                    setSubmitValidation={setSubmitValidation}
                  />
                </div>
                <p className="text-[#dae5ea] text-[17px] text-center mb-10">
                  Ensure all details are correct before submission
                </p>
                <button
                  onClick={() => {
                    setSubmitValidation(true);
                    setFieldTouched(SyncValidationFields.ActivityType, true, false);
                    setFieldTouched(SyncValidationFields.CreditAmount, true, false);
                    setFieldTouched(SyncValidationFields.CreditType, true, false);
                    setFieldTouched(SyncValidationFields.DateCompleted, true, false);
                  }}
                  disabled={isSubmitting || Boolean(errors[AsyncValidationFields.Email])}
                  type="submit"
                  className="m-auto block bg-[#0090a8] py-3 px-11 rounded-full text-[#dae5ea] font-medium disabled:opacity-40"
                >
                  SUBMIT CREDIT
                </button>
              </div>
            </Form>
          )}
        </Formik>
      ) : (
        <SuccessfulSubmit />
      )}
    </div>
  );
};
