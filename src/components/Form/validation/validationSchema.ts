import * as Yup from 'yup';
import { creditFormApi } from '../../../api/submitForm';
import debounce from 'lodash.debounce';
import { AsyncValidationFields, SyncValidationFields } from '../types/form.types';

function asyncDebounce(func: (value: string | undefined) => Promise<boolean>, wait?: number) {
  const debounced = debounce((resolve, reject, value: string | undefined) => {
    func(value).then(resolve).catch(reject);
  }, wait);
  return (value: string | undefined): Promise<boolean> =>
    new Promise((resolve, reject) => {
      debounced(resolve, reject, value);
    });
}

const debounced = asyncDebounce(creditFormApi.validateEmail, 300);

export const getValidationSchema = (isSubmitting: boolean) => {
  return Yup.object({
    [AsyncValidationFields.Email]: Yup.string()
      .test('checkEmailUnique', 'This email is already registered.', async (value, context) => {
        try {
          await Yup.string().required('Required').email('Invalid email').validate(value);

          if (isSubmitting) return true;

          return debounced(value);
        } catch (error) {
          if (error instanceof Yup.ValidationError) {
            return context.createError({ message: error.message });
          }
          return context.createError({ message: 'Unexpected error' });
        }
      })
      .required('Required'),
    [SyncValidationFields.ActivityType]: Yup.string().required('Required'),
    [SyncValidationFields.DateCompleted]: Yup.string().required('Required'),
    [SyncValidationFields.CreditType]: Yup.string().required('Required'),
    [SyncValidationFields.CreditAmount]: Yup.string().required('Required'),
    [SyncValidationFields.Description]: Yup.string().max(1000, 'Max length 1000 symbols'),
    [SyncValidationFields.Reflection]: Yup.string().max(1000, 'Max length 1000 symbols'),
  });
};
