import { FC, useEffect } from 'react';
import Select, { SingleValue } from 'react-select';

type TOption = { value: string; label: string };

interface SelectProps {
  name: string;
  options: TOption[];
  placeholder?: string;
  setFieldValue: (field: string, value: string, shouldValidate?: boolean | undefined) => void;
  validateField: (field: string) => void;
  value: string;
  errors: { [field: string]: string };
  touched: { [field: string]: boolean };
}

export const MySelect: FC<SelectProps> = ({
  options,
  placeholder,
  name,
  setFieldValue,
  validateField,
  value,
  errors,
  touched,
}) => {
  const onSelect = (newValue: SingleValue<TOption>) => {
    setFieldValue(name, newValue?.value || '');
  };

  useEffect(() => {
    validateField(name);
  }, [value]);

  return (
    <div className="relative mb-5">
      <Select
        options={options}
        placeholder={placeholder}
        isSearchable={false}
        onChange={onSelect}
        styles={{
          control: baseStyles => ({
            ...baseStyles,
            backgroundColor: '#eeeeee',
            padding: '6px',
            border: 'none',
            boxShadow: 'none',
            borderRadius: 'unset',
          }),
          placeholder: baseStyles => ({
            ...baseStyles,
            fontWeight: 'bold',
            color: '#212121',
          }),
          indicatorSeparator: () => ({
            display: 'none',
          }),
        }}
      />
      {touched[name] && errors[name] && <div className="text-red-400">{errors[name]}</div>}
    </div>
  );
};
