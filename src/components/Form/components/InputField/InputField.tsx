import { InputHTMLAttributes, useEffect, useState } from 'react';
import { useField } from 'formik';
import cn from 'classnames';
import CompleteIcon from './assets/complete.svg?react';
import Spinner from './assets/spinner.gif';

interface IInputFieldProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  styles?: string;
  checkIcon?: boolean;
  isValidating?: boolean;
  asyncValidating?: boolean;
  setFieldTouched: (
    field: string,
    isTouched?: boolean | undefined,
    shouldValidate?: boolean | undefined
  ) => void;
  validateField: (field: string) => void;
  isSubmitting: boolean;
  setSubmitValidation: (isSubmitValidation: boolean) => void;
}

export const InputField: React.FC<IInputFieldProps> = ({
  name,
  styles,
  checkIcon,
  isValidating,
  asyncValidating,
  setFieldTouched,
  validateField,
  isSubmitting,
  setSubmitValidation,
  ...props
}) => {
  const [field, meta] = useField(name);
  const [shouldValidateField, setShouldValidateField] = useState(false);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSubmitValidation(false);
    setShouldValidateField(true);
    field.onChange(e);
    setFieldTouched(name, true, false);
  };

  useEffect(() => {
    if (field.value || shouldValidateField) {
      validateField(name);
    }
  }, [field.value]);

  const isError = meta.touched && meta.error;

  return (
    <div className="relative mb-5">
      <input
        className={cn(
          'bg-[#eeeeee] pl-4 pr-9 py-3 placeholder:text-slate-400 placeholder:font-medium w-full focus:outline-0',
          { 'outline outline-1 outline-red-400': isError },
          styles
        )}
        {...field}
        {...props}
        onChange={handleChange}
      />
      {isError && <div className="text-red-400">{meta.error}</div>}
      {asyncValidating && isValidating && !isSubmitting && (
        <img src={Spinner} className="absolute right-2 top-6 -translate-y-1/2" />
      )}
      {checkIcon && meta.touched && !meta.error && !isValidating && (
        <CompleteIcon className="absolute right-2 top-6 -translate-y-1/2" />
      )}
    </div>
  );
};
