import SuccessSubmitIcon from './assets/successSubmit.svg?react';

export const SuccessfulSubmit = () => {
  return (
    <div className="w-full max-w-full lg:max-w-md min-w-full sm:min-w-[350px] md:min-w-[450px] lg:min-w-[550px] py-20 px-5 max-h-96 bg-slate-300 flex flex-col justify-center items-center">
      <SuccessSubmitIcon className="mb-8" />
      <h3 className="text-2xl sm:text-3xl md:text-4xl text-center mb-5">Thank you!</h3>
      <p className="text-lg md:text-xl text-center">Your form has been submitted</p>
    </div>
  );
};
