export enum AsyncValidationFields {
  Email = 'email',
}

export enum SyncValidationFields {
  ActivityType = 'activityType',
  DateCompleted = 'dateCompleted',
  CreditType = 'creditType',
  CreditAmount = 'creditAmount',
  Description = 'description',
  Reflection = 'reflection',
}

export interface IInitialValues {
  [AsyncValidationFields.Email]: string;
  [SyncValidationFields.ActivityType]: string;
  [SyncValidationFields.DateCompleted]: Date | null;
  [SyncValidationFields.CreditType]: string;
  [SyncValidationFields.CreditAmount]: string;
  [SyncValidationFields.Description]: string;
  [SyncValidationFields.Reflection]: string;
}

export interface ICreditFormData {
  [SyncValidationFields.ActivityType]: string[];
  [SyncValidationFields.CreditType]: string[];
  [SyncValidationFields.CreditAmount]: string[];
}
